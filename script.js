const axios = require('axios');
const Path = require('path');
const Fs = require('fs');
const request = require('request');
const mailer = require("nodemailer");

const smtpTransport = mailer.createTransport({
    service: "Gmail",
    auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_PASSWORD
    }
});

async function getUrlsToFetch() {
    try {
        let res = await axios.get(process.env.REMOTE_URL);
        if(res.status === 200) {
            console.log(`successfully fetched data from ${process.env.REMOTE_URL}`);
            return res.data;
        }
    } catch(err) {
        console.log(`request to ${process.env.REMOTE_URL} failed with statusCode ${err.response.status}`)
        return 
    }
};


async function fetchUrl(url) {
    try {
        let response = await axios({
            method: 'GET',
            url: url,
            responseType: 'stream'
        });
        let fileName = url.split('/').slice(-1)[0];
        response.data.pipe(Fs.createWriteStream(Path.resolve(__dirname, fileName)));
        return new Promise((resolve, reject) => {
            response.data.on('end', () => {
              console.log(`successfully saved file with the name ${fileName}`);
              resolve(fileName);
            });
            response.data.on('error', () => {
              reject();
            });
        });
    } catch(err) {
        console.log(`error happened with url ${url} , file not saved`);
        return null;
    }
}

async function processRequests() {
    try {
        let urls = await getUrlsToFetch();
        if(urls && urls.length > 0) {
            let fileNames= [];
            for(let i = 0; i< urls.length; i++) {
                let fileNameAfterDownload = await fetchUrl(urls[i]) ;
                if(fileNameAfterDownload && fileNameAfterDownload.length > 0) {
                    if(sendFile(fileNameAfterDownload, urls[i])) {
                        fileNames.push(fileNameAfterDownload);
                    };
                }
            }
            sendEmail(fileNames);
        }
    } catch(err) {
        console.log('error in processRequests');
        console.log(err);
    }
};


async function sendFile(fileName, url) {
    console.log(`sending file with the fileName ${fileName}`);
    try {
        const options = {
            method: "POST",
            url: `http://${process.env.IP}:${process.env.PORT}${process.env.SEND_FILE_PATH}`,
            formData : {
                "file" : Fs.createReadStream(Path.resolve(__dirname,fileName)),
                "fileUrl" : url
            }
        };
       let res = await request(options, function (err, res, body) {
            if(err) {
                console.log(`file with fileName ${fileName} was not saved on the remote server`);
                return false;
            }
            return true;
        });
        return res;
    } catch(err) {
        console.log(`error happened in sendFile with fileName ${fileName}`);
        console.log(err);
    }
};

function sendEmail(fileNames) {
    let mail = {
        from: "irscrdummy@gmail.com",
        to: "irscrdummy@gmail.com",
        subject: "Following files have been uploaded",
        text: '',
        html: `<b>${fileNames.join('<br>')}</b>`
    };

    smtpTransport.sendMail(mail, function(error, response){
        if(error) {
            console.log(error);
        } else {
            console.log("Email message sent successfully");
        }
        smtpTransport.close();
    }); 
};



function runScript() {
    processRequests();
    setTimeout(runScript, process.env.RETRY_TIME);
}



runScript();
