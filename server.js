const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const Path = require('path');
const busboy = require('connect-busboy');

module.exports = () => {
    const server = express();
    const Fs = require('fs');
    const storage = multer.diskStorage(
        {
            destination: './uploads/',
            filename: function ( req, file, cb ) {
                console.log(file);
                if(typeof savedFiles[file.originalname] !== 'undefined') {
                    
                    console.log(`new fileName is ${fileName}`);
                } else {
                    savedFiles[file.originalname] = 1;
                }
                console.log(savedFiles);
                cb( null, file.originalname);
            }
        }
    );
    const upload = multer( { storage: storage } );

    let savedFiles = {};


    let init = () => {
        server.use(bodyParser.json());
        server.use(busboy()); 
        server.use(bodyParser.urlencoded({ extended: true }));
        
    };

    let start = () => {
        server.listen(process.env.PORT, process.env.IP,() => {
            console.log(`server listening on ${process.env.IP}:${process.env.PORT}`);
        });

        server.post('/saveFile', (req, res) => {
            let url = '';
            req.pipe(req.busboy);
            req.busboy.on('field', function(key, value) {
                url = value;
                console.log(value);
            });

            req.busboy.on('file', function (fieldname, file, filename) {
                if(typeof savedFiles[filename] !== 'undefined') {
                    let name = filename.split('.');
                    let suffix = filename.pop();
                    name = `${filename}_${savedFiles[filename]}`;
                    name = name + suffix;
                    savedFiles[name] = url;
                } else {
                    savedFiles[filename] = url;
                }
                fstream = Fs.createWriteStream(__dirname + '/files/' + filename);
                file.pipe(fstream);
                fstream.on('close', function () {
                    res.status(200).send();
                });
            }); 
        });

   }
    
   return {
       init: init,
       start:start
   };
  
}



